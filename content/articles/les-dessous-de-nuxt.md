---
title: Les dessous de Nuxt
description: Feedback after 3 years of nuxt
img: /main/cacbg2.jpg
alt: my first blog post
lang: fr
version-en: nuxt-insights
author: polo
---

## Le projet

Lorem ipsum

## Les difficultés

### Plusieurs projets en un

Lorem ipsum

```js
export default {
  props: {
    toc: {
      type: Array,
      default: () => [],
    },
  },
}
```

### Les modules

Lorem ipsum

## La communauté

Lorem
