# My **skills**

---

I am a full-stack developer, Java SE/EE specialist but also PHP with Laravel. I enjoy myself in this fast paced environment which is the Javascript world, testing new frameworks (VueJS, React, AngularJS), platforms (NodeJS), admiring the tools gushing out of this boiling world... (Github webhooks, Gitlab CI, Gulp)
