# Hire **me!**

---

I am a creative person focusing on clean code, discovering new frameworks and technologies.

<pld-button href="mailto:plduhoux@gmail.com">Contact me</pld-button>
