---
trotblogger:
  name: TrotBlogger
  image: /projects/trotblogger-96.png
  link: https://trotblogger.com
autourdelorangebleue:
  name: Autour de l'Orange Bleue
  image: /projects/aob-96.png
  link: https://autourdelorangebleue.com
  description: ''
allmangasreader:
  name: All Mangas Reader
  image: /projects/amr-96.png
  link: https://autourdelorangebleue.com
---

# My **projects**

---

<project :project="trotblogger">
A blogging platform for travelers, enjoy how easy it is to create a beautiful and responsive blog, mixing maps, photos and videos wherever you are.
</project>
<project :project="autourdelorangebleue">
The blog of our road trip in family through America (17 countries in 3 years)
</project>
<project :project="allmangasreader">

Read and follow your favorite mangas as in your book store.

All Mangas Reader contains three main projects : the extension, the supported website interfaces, and the websites tester.

We are planning to create a desktop application and a mobile application (even if the extension is working fine in Firefox for Android). Here is AMR's patreon to support the project !
</project>
