# About **me**

---

Traveler and code addict, I spent the last three years on a road trip with my family from Canada to Argentina. During this nomad life, I developed the blogging platform **TrotBlogger** which allows travelers to easily create beautiful and responsive websites including maps and medias.

**Autour de l'Orange Bleue**, the blog of our 3 years in America, has been made using the platform.

A few years ago, I created All Mangas Reader, a browser extension to read mangas online, which encounter a great success.

I really enjoy creating mobile apps with NativeScript.

I love to spend family time with my three daughters.
