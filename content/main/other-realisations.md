# Other **realizations**

---

- Where and When : a mocked app which allows you to look for a destination for your next vacations, knowing - or not - in which part of the world you want to travel and knowing - or not - when you will leave (developed in NativeScript : code)
- Random Pedia : displays multiple pages from Wikipedia chosen randomly (developed in NativeScript : code)
- Face detect : a canvas experiment to crop an image file on any shape
