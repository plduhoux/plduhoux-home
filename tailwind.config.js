/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    fontFamily: {
      sans: '"Open Sans", sans-serif',
      display: '"Open Sans", sans-serif',
      body: '"Open Sans", sans-serif',
    },
    extend: {
      colors: {},
    },
  },
  variants: {},
  plugins: [],
}
